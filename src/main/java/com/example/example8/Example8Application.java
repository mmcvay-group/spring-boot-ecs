package com.example.example8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Example8Application {

	//private String name;


	public static void main(String[] args) {
		SpringApplication.run(Example8Application.class, args);
	}

	@GetMapping("/")
	String home() {
		return "Changing text!";
	}

    private int foo(int a) {
  		int b = 12;
  		if (a == 1) {
    		return b;
  		}
  
  		return b;
	}


	void nonCompliantSwitch() {

		int myVariable = 1;

		switch (myVariable) {
			case 1:
			  foo(myVariable);
			  break;
			case 2:  // Both 'doSomething()' and 'doSomethingElse()' will be executed. Is it on purpose ?
			  doSomething();
			default:
			  doSomethingElse();
			  break;
		  }
	}

	private void doSomething() {}

	private void doSomethingElse() {}

}
